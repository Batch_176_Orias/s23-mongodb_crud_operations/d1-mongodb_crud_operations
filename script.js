//crud operation(Mongo DB)

//create -- insertion of data

// for single insert of record
db.users.insertOne({
	"firstName": "John",
	"lastName": "Doe",
	"age": 25,
	"email": "email123@elail.com",
	"department": "none"
})

db.trial.insertOne({
	"name": "Snake",
	"type": "soldier",
	"rank": "Big Boss"
})

// multiple insert of record

db.mgs.insertMany([
		{
			"name": "Raiden",
			"type": "soldier",
			"rank": "S"
		},
		{
			"name": "Boss",
			"type": "soldier",
			"rank": "S++"
		}

	])

db.users.insertMany([
		{
			"firstName": "X",
			"lastName": "Reploid",
			"age": 25,
			"email": "email123@elail.com",
			"department": "none"
		},
		{
			"firstName": "Zero",
			"lastName": "Reploid",
			"age": 25,
			"email": "email123@elail.com",
			"department": "none"
		}
	])

//mini act
// - make a new collection with the name "coursers"
// - insert the following fields and values

/*
	name: javascript 101,
	price: 5000,
	description: Introduction to javascript
	isActive: true,

	name: HTML 101,
	price: 2000,
	description: Introduction to HTML,
	isActive: true

	name: CSS 101,
	price: 2500,
	description: Introduction to CSS
	isActive: false
*/

db.courses.insertMany([
		{
		    name: "javascript 101",
			price: 5000,
			description: "Introduction to javascript",
			isActive: true
		     },
		     {
		        name: "HTML 101",
			price: 2000,
			description: "Introduction to HTML",
			isActive: true
		     },
		     {
		        name: "CSS 101",
			price: 2500,
			description: "Introduction to CSS",
			isActive: false
		     }
	])

//Read(select) retrieves record

db.courses.find(); //return all records
db.courses.find({ //finds all within the criteria
	"price": 2000
})
db.courses.findOne({ //finds the first within the criteria
	"price": 2000
})
db.courses.findOne({}) //return the first record

//update -- modify record's info

db.courses.updateOne( 	//update one record. If set property value does not exist, the field will be added to the collection
		{
			"price": 2000
		},
		{
			$set: {
				"price": 3000
			}
		}
	)

db.courses.updateMany( 	//update all matched record. If set property value does not exist, the field will be added to the collection
		{
			"price": 2000
		},
		{
			$set: {
				"price": 3000
			}
		}
	)
//removing a property with update

db.courses.updateMany(
		{
			"total": 2500
		},
		{
			$unset: {
				"total":2500	
			}
		}
	)

//05-04-2022

db.products.insertMany([
		{
			name: "Xperia Pro I",
			price: 60000
		},
		{
			name: "Iphone 13",
			price: 55000
		}
	])

	db.products.find();
	db.products.findOne({});
	db.products.updateOne({
		name: "Xperia 1 iv"},
		{$set: {
			price: 53000
		}}
	);
	db.products.updateMany(
			{},
			{$set: {
				isActive:true
			}}
		)

	//delete - removes record(s)
	db.products.deleteOne({
                name: "Iphone 13"
            })
	db.products.deleteMany({
                isActive: false
            })

	db.products.deleteOne({
                name: "Iphone 13",
                isActive:true
            })

	db.products.deleteMany({});